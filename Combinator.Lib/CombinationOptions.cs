﻿namespace Combinator.Lib
{
    /// <summary>
    /// Various options when validating parts to compare
    /// </summary>
    public enum CombinationOptions
    {
        /// <summary>
        /// Leave the part, it cannot be used to form the desired combination
        /// </summary>
        Leave,
        /// <summary>
        /// Keep it, it may be used
        /// </summary>
        TakeLater,
        /// <summary>
        /// Take it!
        /// </summary>
        Take,

    }
}