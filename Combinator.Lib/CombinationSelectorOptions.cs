﻿namespace Combinator.Lib
{
    /// <summary>
    /// Various item-level options when handling an ICombinable collection
    /// </summary>
    public enum CombinationSelectorOptions
    {
        /// <summary>
        /// This is a combination to look for
        /// </summary>
        LookFor,
        /// <summary>
        /// Use as fragment to build combinations
        /// </summary>
        Use,
        /// <summary>
        /// Drop it from the source
        /// </summary>
        Nope,

    }
}