﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks.Dataflow;

namespace Combinator.Lib
{
    /// <summary>
    /// Various extension methods on IEnumerable of ICombinable (of T) 
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Look for combinations for all items found in the same input using the provided 
        /// selector
        /// </summary>
        /// <param name="input">Input fragments</param>
        /// <param name="allowRepetitions">Allow fragments to repeat themselves (within combinations as well as targets)</param>
        /// <param name="selector">Selector function that determines fragments and targets</param>
        /// <returns>All targets and their possible combinations both taken from the input collection</returns>
        public static IEnumerable<ICombinable<T>> FindCombinations<T>(this IEnumerable<ICombinable<T>> input, Func<ICombinable<T>, CombinationSelectorOptions> selector, bool allowRepetitions = true)
        {
            if (input == null || !input.Any())
                return null;

            var targets = new List<T>();
            var fragments = new List<ICombinable<T>>();

            foreach (var fragment in input)
            {
                var option = selector.Invoke(fragment);
                if (option == CombinationSelectorOptions.Nope)
                    continue;
                if (option == CombinationSelectorOptions.LookFor)
                    targets.Add(fragment.Value);
                fragments.Add(fragment);
            }
            var result = new List<ICombinable<T>>();
            var targetsBlock = new ActionBlock<T>((target) =>
            {
                var x = fragments.FindCombinations(target, allowRepetitions);
                lock(result)
                    result.AddRange(x);
            }, new ExecutionDataflowBlockOptions()
            {
                MaxDegreeOfParallelism = 100 
            });

            foreach (var target in (allowRepetitions ? targets : targets.Distinct()))
                targetsBlock.Post(target);

            targetsBlock.Complete();
            targetsBlock.Completion.Wait();

            return result;
        }

        /// <summary>
        /// Finds all possible combinations from the input that make up the provided word.
        /// </summary>
        /// <param name="input">Input fragments</param>
        /// <param name="allowRepetitions">Allow fragments to repeat themselves within one combination</param>
        /// <param name="value">The value for which combinations need to be found</param>
        /// <returns>List of combinations (if any)</returns>
        public static IEnumerable<ICombinable<T>> FindCombinations<T>(this IEnumerable<ICombinable<T>> input, T value, bool allowRepetitions = true)
        {
            var fragments = new List<ICombinable<T>>();
            var remaining = new List<ICombinable<T>>();
            foreach (var fragment in input)
            {
                var option = fragment.Validate(value);
                if (option == CombinationOptions.Leave)
                    continue;
                if (option == CombinationOptions.Take)
                {
                    fragments.Add(fragment);
                    if (allowRepetitions)
                        remaining.Add(fragment);
                }
                else if (option == CombinationOptions.TakeLater)
                    remaining.Add(fragment);
            }
            var result = new List<ICombinable<T>>();
            var takeFragmentsAsRemaining = !remaining.Any() && !allowRepetitions;
            foreach (var fragment in fragments.Distinct())
            {
                var (next, incomplete) = fragment.RequiredForCompletion(value);
                if (takeFragmentsAsRemaining)
                    remaining = fragments.Except(new[] { fragment }).ToList();
                if (incomplete && remaining.Any())
                {
                    var combinations = remaining.FindCombinations(next, allowRepetitions);
                    result.AddRange(combinations.Select(c => fragment.CombineWith(c)));
                }
                else
                {
                    result.Add(fragment);
                }
            }
            return result.Where(r => r.Value.Equals(value));
        }
    }
}
