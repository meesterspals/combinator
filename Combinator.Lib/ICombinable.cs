﻿using System;
using System.Collections.Generic;

namespace Combinator.Lib
{
    /// <summary>
    /// The definition of a combinable item. Combinable items provide means to both combine and
    /// uniquely identify themselves by means of an arbitrary hash value.
    /// </summary>
    /// <typeparam name="T">Type of the actual payload value</typeparam>
    public interface ICombinable<T> : IEquatable<ICombinable<T>>
    {
        
        /// <summary>
        /// Value payload
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Gets the components that make up this combination
        /// </summary>
        IEnumerable<ICombinable<T>> Components { get; }

        /// <summary>
        /// Create a combination of this and other
        /// </summary>
        /// <param name="other">Other combinable</param>
        /// <returns>New combinable that is the combination of this and other</returns>
        ICombinable<T> CombineWith(ICombinable<T> other);

        /// <summary>
        /// Hash for the actual combination that generates the value. 
        /// </summary>
        int CombinationHash { get; }

        /// <summary>
        /// Validates whether or not this combination could be used to form part 
        /// </summary>
        /// <param name="part"></param>
        /// <returns>How this combinable may or may not be used to form part</returns>
        CombinationOptions Validate(T part);

        /// <summary>
        /// Get the remaining required value to complete this combinable in order to form part
        /// </summary>
        /// <param name="part"></param>
        /// <returns>Remaining part and flag indicating if anything actually remains</returns>
        (T, bool) RequiredForCompletion(T part);
    }
}
