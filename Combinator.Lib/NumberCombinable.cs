﻿using System.Collections.Generic;
using System.Linq;

namespace Combinator.Lib
{
    /// <summary>
    /// Combinable for numbers (int)
    /// </summary>
    public class NumberCombinable : SourcedCombinableBase<int>
    {

        public NumberCombinable(int value, int sourceIndex) : base(value, sourceIndex)
        {
        }

        public NumberCombinable(IEnumerable<ICombinable<int>> components) : base(components)
        {

        }

        public override (int, bool) RequiredForCompletion(int value)
        {
            if (this.Value >= value)
                return (0, false);
            return (value - this.Value, true);
        }

        protected override string CalculateHashValue(IEnumerable<ICombinable<int>> components)
        {
            return components.Select(c => c.Value.ToString())
                             .OrderBy(v => v)
                             .Aggregate((a, b) => $"{a}+{b}");
        }

        public override CombinationOptions Validate(int value)
        {
            if (this.Value > value)
                return CombinationOptions.Leave;
            return CombinationOptions.Take;
        }

        protected override int CombineValues(int a, int b)
        {
            return a + b;
        }

        protected override SourcedCombinableBase<int> CreateNew(ICombinable<int> other)
        {
            return new NumberCombinable(this.Components.Concat(other.Components).ToList());
        }
    }
}
