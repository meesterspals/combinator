﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Combinator.Lib
{
    /// <summary>
    /// Base for a combinable that is sourced (i.e. includes a source index) and most
    /// common boilerplate code. Equality is based on the hashvalue of the HashValue property
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SourcedCombinableBase<T> : ICombinable<T>
    {
        public SourcedCombinableBase(T value, int sourceIndex)
        {
            Value = value;
            SourceIndex = sourceIndex;
            this.Components = new List<ICombinable<T>>() { this };
            HashValue = CalculateHashValue(Components);
        }

        protected SourcedCombinableBase(IEnumerable<ICombinable<T>> components)
        {
            Components = components;
            Value = Components.Select(c => c.Value).Aggregate((a, b) => CombineValues(a, b));
            HashValue = CalculateHashValue(Components);
        }

        protected string HashValue { get; private set; }

        protected virtual string CalculateHashValue(IEnumerable<ICombinable<T>> components)
        {
            return components.Select(c => c.Value.ToString()).Aggregate((a, b) => $"{a}+{b}");
        }

        protected abstract SourcedCombinableBase<T> CreateNew(ICombinable<T> other);

        protected abstract T CombineValues(T a, T b);

        public abstract (T, bool) RequiredForCompletion(T value);

        public abstract CombinationOptions Validate(T value);

        public virtual int CombinationHash => HashValue.GetHashCode();

        /// <summary>
        /// Source index - marks the original input location of the combinable.
        /// </summary>
        public int SourceIndex { get; private set; }

        public virtual T Value
        {
            get;
            protected set;
        }

        public virtual IEnumerable<ICombinable<T>> Components { get; protected set; }
                
        public ICombinable<T> CombineWith(ICombinable<T> other)
        {
            if (other == null)
                return this;

            return CreateNew(other);
        }

        public override int GetHashCode()
        {
            return CombinationHash;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as ICombinable<T>);
        }

        public bool Equals(ICombinable<T> other)
        {
            if (other == null)
                return false;
            return this.CombinationHash == other.CombinationHash;
        }

        public override string ToString()
        {
            var components = Components.Select(c => c.Value.ToString()).Aggregate((a, b) => $"{a}+{b}");
            return $"{components} => {Value}";
        }
    }
}
