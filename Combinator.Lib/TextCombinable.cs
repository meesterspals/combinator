﻿using System.Collections.Generic;
using System.Linq;

namespace Combinator.Lib
{
    /// <summary>
    /// Sourced combinable with text (string) payload
    /// </summary>
    public class TextCombinable : SourcedCombinableBase<string>
    {

        public TextCombinable(string value, int sourceIndex) : base(value, sourceIndex)
        {
        }

        public TextCombinable(IEnumerable<ICombinable<string>> components) : base(components)
        {

        }

        protected override string CombineValues(string a, string b)
        {
            return a + b;
        }

        protected override SourcedCombinableBase<string> CreateNew(ICombinable<string> other)
        {
            return new TextCombinable(this.Components.Concat(other.Components).ToList());
        }

        public override CombinationOptions Validate(string value)
        {
            if (this.Value.Length > value.Length || !(value.Contains(this.Value)))
                return CombinationOptions.Leave;
            if (value.StartsWith(this.Value))
                return CombinationOptions.Take;
            return CombinationOptions.TakeLater;
        }

        public override (string, bool) RequiredForCompletion(string value)
        {
            if (value.Length == this.Value.Length)
                return (null, false);
            return (value[this.Value.Length..], true);
        }
    }
}
