using Combinator.Lib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Combinator.Tests
{
    public class Tests
    {
        private async Task<IEnumerable<ICombinable<string>>> LoadAsync(string fileName)
        {
            var sourcePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), $"../../../{fileName}");
            int index = 0;
            return (await File.ReadAllLinesAsync(sourcePath).ConfigureAwait(false)).Select(l => new TextCombinable(l, index++)).ToList();
        }

        [Test]
        public async Task Combofinder()
        {
            var input = await LoadAsync("input.txt").ConfigureAwait(false);

            var combosWith = input.FindCombinations("google");
            var combosWithout = input.FindCombinations("google", false);

            Assert.AreEqual(combosWith.Count(), 7);
            Assert.AreEqual(combosWithout.Count(), 5);

            var tuples = combosWithout.Where(c => c.Components.Count() == 2);
            Assert.AreEqual(tuples.Count(), 3);

            Assert.AreEqual(combosWith.Where(c => c.Components.Count() == 5).Count(), 1);
        }

        [Test]
        public void Numbers()
        {
            var input = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 }.Select(i => new NumberCombinable(i, i));

            var combos = input.FindCombinations(10, false);

            var doubles = combos.Where(c => c.Components.Count() == 2);

            Assert.AreEqual(combos.Distinct().Count(), 10); // 9 sums make 10, including 10 = 10 total

        }

        [Test]
        public async Task All()
        {
            var input = await LoadAsync("large.txt").ConfigureAwait(false);

            var combos = input.FindCombinations((combo) =>
            {
                if (combo.Value.Length == 6)
                    return CombinationSelectorOptions.LookFor;
                if (combo.Value.Length < 6)
                    return CombinationSelectorOptions.Use;
                return CombinationSelectorOptions.Nope;
            }, false);

            combos.ToList().ForEach(t => Console.WriteLine(t));
            Console.WriteLine($"Found {combos.Count()} unique combinations for distinct 6-letter words.");

        }


        [Test]
        public async Task TheTask()
        {
            var input = await LoadAsync("large.txt").ConfigureAwait(false);

            var combos = input.FindCombinations((combo) =>
            {
                if (combo.Value.Length == 6)
                    return CombinationSelectorOptions.LookFor;
                if (combo.Value.Length < 6)
                    return CombinationSelectorOptions.Use;
                return CombinationSelectorOptions.Nope;
            }, false);

            var tuples = combos.Where(c => c.Components.Count() == 2).Distinct().ToList();

            tuples.ForEach(t => Console.WriteLine(t));
            Console.WriteLine($"Found {tuples.Count} unique 2-part combinations for distinct 6-letter words.");

        }
    }
}