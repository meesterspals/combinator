# Combinator

Some combinatorial test code. Basic abstraction `ICombinable<T>` and some extension methods on `IEnumerable<ICombinable<T>>` that
perform rudimentary combination searches. Includes a Text and Number implementation to illustrate how combinations of different data types 
can be implemented.

## Make this project do something

Unit tests can be run using `dotnet test`

Run `dotnet test --nologo -l "console;verbosity=detailed"` to include console output generated from the tests.

